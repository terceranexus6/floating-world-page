use Rack::Static,
  :urls => ["/images", "/js", "/css"],
  :root => "public"

map "/" do
  run lambda { |env|
  [
    200, 
    {
      'Content-Type'  => 'text/html', 
      'Cache-Control' => 'no-cache, must-revalidate' 
    },
    File.open('public/index.html', File::RDONLY)
  ]
}
end

map "/faq" do
  run lambda { |env|
  [
    200, 
    {
      'Content-Type'  => 'text/html', 
      'Cache-Control' => 'no-cache, must-revalidate' 
    },
    File.open('public/faq.html', File::RDONLY)
  ]
}
end
